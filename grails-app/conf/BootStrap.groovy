/* 
 Copyright (c) 2015 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     BootStrap.groovy
 Description:  Authentication Filter, checks for valid access to resources
 Project:      PremoDemo
 Template: grails-24x/Bootstrap.groovy.vmg
 */



import grails.converters.JSON;

import com.generatron.premodemo.*;
import com.generatron.premodemo.*;

import org.codehaus.groovy.grails.plugins.springsecurity.ui.*;

class BootStrap {
	def springSecurityService;
	def init = { servletContext ->
	}
	def destroy = {
	}
}


/* 
 [TRIVIA]
 It would take a person typing  @ 100.0 cpm, 
 approximately 3.14 minutes to type the 314+ characters in this file.
 */



