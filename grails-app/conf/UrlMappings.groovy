/* 
 Copyright (c) 2015 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     UrlMappings.groovy
 Description:  URL Mappings
 Project:      PremoDemo
 Template: grails-24x/URLMappings.groovy.vmg
 */

class UrlMappings {
	static mappings = {
		"/$controller/$action?/$id?(.$format)?"{ constraints { // apply constraints here
			} }

		"/"(view:"/index")
		"500"(view:'/error')

		//routes for User
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/user/$id"(controller:"UserApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/user"(controller:"UserApi"){
			action = [GET:"index", POST:"create"]
		}

		"/api/user/$id/addFriendshipToUser"(controller:"UserApi",action:"addFriendshipToUser")
		"/api/user/$id/removeFriendshipFromUser"(controller:"UserApi",action:"removeFriendshipFromUser")
		"/api/user/$id/addPodcastToUser"(controller:"UserApi",action:"addPodcastToUser")
		"/api/user/$id/removePodcastFromUser"(controller:"UserApi",action:"removePodcastFromUser")

		//routes for Podcast
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/podcast/$id"(controller:"PodcastApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/podcast"(controller:"PodcastApi"){
			action = [GET:"index", POST:"create"]
		}


		//routes for Friendship
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/friendship/$id"(controller:"FriendshipApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/friendship"(controller:"FriendshipApi"){
			action = [GET:"index", POST:"create"]
		}



	}
}

/* 
 [TRIVIA]
 It would take a person typing  @ 100.0 cpm, 
 approximately 16.53 minutes to type the 1653+ characters in this file.
 */



