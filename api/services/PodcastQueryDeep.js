/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     PodcastQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      PremoDemo
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var PodcastPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                podcast: function(cb) {
                    Podcast.findOne({
                        where: {
                            id: id
                        }
                    }).populate("author").exec(cb);
                }


                //Relations for author
                ,
                friendssForauthor: ['podcast',
                    function(cb, results) {
                        Friendship.find({
                            id: _.pluck(results.podcast.author, 'friends')
                        }).exec(cb);
                    }
                ],
                authorDeep: ['friendssForauthor',
                    function(cb, results) {

                        var friendss = _.indexBy(results.friendssForauthor, 'id');

                        var podcast = results.podcast.toObject();
                        podcast.authorMapped = _.map(podcast.author, function(user) {
                            user.friends = friendss[user.friends];
                            return user;
                        });

                        return cb(null, podcast);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = PodcastPopulateDeepService;


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 14.4 minutes to type the 1440+ characters in this file.
 */