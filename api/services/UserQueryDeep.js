/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     UserQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      PremoDemo
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var UserPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                user: function(cb) {
                    User.findOne({
                        where: {
                            id: id
                        }
                    }).populate("friends").populate("podcasts").exec(cb);
                }


                //Relations for friends
                ,
                friendsDeep: [,
                    function(cb, results) {


                        var user = results.user.toObject();
                        user.friendsMapped = _.map(user.friends, function(friendship) {
                            return friendship;
                        });

                        return cb(null, user);
                    }
                ]


                //Relations for podcasts
                ,
                podcastsDeep: [,
                    function(cb, results) {


                        var user = results.user.toObject();
                        user.podcastsMapped = _.map(user.podcasts, function(podcast) {
                            return podcast;
                        });

                        return cb(null, user);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = UserPopulateDeepService;


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 13.58 minutes to type the 1358+ characters in this file.
 */