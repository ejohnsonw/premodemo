/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Filename:     UserApiTest.java
Description:  Creates a Retrofit based Interface
Project:      PremoDemo
Template: Android/RetroFit/TestFixture.vm
 */

package  com.generatron.premodemo
public class UserApiTest {

String API = "<put api base url here>";
RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL)setEndpoint(API).build(); 
PremoDemoApi api = restAdapter.create(PremoDemoApi.class);

//routes for User

public void testGetUsers(){
}

public void testCreateUser(){
}

public void testRetrieveUser(){
}


public void  testUpdateUser(){
}


public void testDeleteUser(){
}

//Per attribute

}
	

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 4.87 minutes to type the 487+ characters in this file.
 */


