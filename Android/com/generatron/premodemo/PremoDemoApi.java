/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Filename:     PremoDemoApi.java
Description:  Creates a Retrofit based Interface
Project:      PremoDemo
Template: Android/RetroFit/webserviceclient.vmg
 */
package  com.generatron.premodemo
public interface PremoDemoApi {
//From https://guides.codepath.com/android/Consuming-APIs-with-Retrofit
//http://themakeinfo.com/2015/04/retrofit-android-tutorial/

//routes for Friendship
@GET("/friendship/") 
public Call<List<Friendship>> getFriendships();

@POST("/friendship/") 
public Call<List<Friendship>> createFriendship(@Body Friendship friendship);

@GET("/friendship/{id}")
public Call<Friendship> retrieveFriendship(@Path("id") String id);

@POST("/friendship/{id}")
public Call<Friendship> updateFriendship(@Path("id") String id, @Body Friendship friendship);

@DELETE("/friendship/{id}")
public Call<Friendship> deleteFriendship(@Path("id") String id);



//routes for Podcast
@GET("/podcast/") 
public Call<List<Podcast>> getPodcasts();

@POST("/podcast/") 
public Call<List<Podcast>> createPodcast(@Body Podcast podcast);

@GET("/podcast/{id}")
public Call<Podcast> retrievePodcast(@Path("id") String id);

@POST("/podcast/{id}")
public Call<Podcast> updatePodcast(@Path("id") String id, @Body Podcast podcast);

@DELETE("/podcast/{id}")
public Call<Podcast> deletePodcast(@Path("id") String id);



//routes for User
@GET("/user/") 
public Call<List<User>> getUsers();

@POST("/user/") 
public Call<List<User>> createUser(@Body User user);

@GET("/user/{id}")
public Call<User> retrieveUser(@Path("id") String id);

@POST("/user/{id}")
public Call<User> updateUser(@Path("id") String id, @Body User user);

@DELETE("/user/{id}")
public Call<User> deleteUser(@Path("id") String id);



}

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 15.379999 minutes to type the 1538+ characters in this file.
 */


